package katie.alcoholic;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

import katie.alcoholic.helper.DataManager;
import katie.alcoholic.helper.Helper;
import katie.alcoholic.model.Beer;
import katie.alcoholic.model.Cocktail;
import katie.alcoholic.model.DataModel;

/**
 * Created by liuyi on 2015-09-06.
 */
public class AddCollectionActivity extends AddItemActivity {
    private static final String TAG = "AddCollectionActivity";

    private DataModel existingItem = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_collection);

        existingItem = (DataModel) getIntent().getSerializableExtra(Helper.EXISTING_DATA);

        Spinner spinner = (Spinner) findViewById(R.id.spinner_type);
        if (existingItem != null) { // update
            if (existingItem.isWish) { // wish item
                spinner.setEnabled(false);

                EditText name = (EditText) findViewById(R.id.text_name);
                name.setText(existingItem.getName());

                loadExistingImg(existingItem.getImagePath());

                EditText location = (EditText) findViewById(R.id.text_location);
                location.setText(existingItem.getLocation());
            } else { // existing collection item
                spinner.setEnabled(false);

                EditText name = (EditText) findViewById(R.id.text_name);
                name.setText(existingItem.getName());

                RatingBar rating = (RatingBar) findViewById(R.id.rating);
                rating.setRating(existingItem.getRating());

                loadExistingImg(existingItem.getImagePath());

                EditText location = (EditText) findViewById(R.id.text_location);
                location.setText(existingItem.getLocation());

                EditText tag = (EditText) findViewById(R.id.text_tag);
                tag.setText(existingItem.getTag());

                EditText description = (EditText) findViewById(R.id.text_description);
                description.setText(existingItem.getDescription());
            }
        } else { // create
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO: if is beer, change text to 'Enter brewery'/'flavor'; otherwise, change to 'Enter best location'/'ingredient'
                    isBeer = (position == 0);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }

    private void loadExistingImg(String imgPath) {
        if (imgPath.length() > 0) {
            ImageView picture = (ImageView) findViewById(R.id.picture);
            ImageLoader.getInstance().displayImage(
                    Uri.fromFile(new File(imgPath)).toString(),
                    picture,
                    Helper.defaultOptions);
            imageFile = new File(imgPath);
        }
    }

    public void onSaveClicked(View v) {
        String name = ((EditText) findViewById(R.id.text_name)).getText().toString();
        String location = ((EditText) findViewById(R.id.text_location)).getText().toString();
        String tag = ((EditText) findViewById(R.id.text_tag)).getText().toString();
        String description = ((EditText) findViewById(R.id.text_description)).getText().toString();
        float rating = ((RatingBar) findViewById(R.id.rating)).getRating();

        if (name == "" || imageFile == null) {
            promptLackInfoAlert("Please at lease give a name and a picture.");
        } else {
            Intent resultIntent = null;

            if (existingItem != null) {
                existingItem.setName(name);
                existingItem.setLocation(location);
                existingItem.setRating(rating);
                existingItem.setDescription(description);
                existingItem.setTag(tag);

                // check if need to update image file
                if (!existingItem.getImagePath().equals(imageFile.getAbsolutePath())) {
                    existingItem.finalizeImgFile(imageFile, imgNeedRename);
                }

                existingItem.updateToDB();

                resultIntent = new Intent();
                resultIntent.putExtra(Helper.EXISTING_TIMESTAMP, existingItem.getTimestamp());
            } else {
                DataModel item;
                if (isBeer) {
                    item = new Beer(name, description, imageFile, imgNeedRename, location, rating, tag);
                } else {
                    item = new Cocktail(name, description, imageFile, imgNeedRename, location, rating, tag);
                }
                item.saveToDB();
            }

            setResult(RESULT_OK, resultIntent);
            finish();
        }
    }

}
