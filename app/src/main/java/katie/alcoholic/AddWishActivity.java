package katie.alcoholic;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import katie.alcoholic.model.Beer;
import katie.alcoholic.model.Cocktail;
import katie.alcoholic.model.DataModel;

/**
 * Created by liuyi on 2015-09-12.
 */
public class AddWishActivity extends AddItemActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_wish);
        Spinner spinner = (Spinner) findViewById(R.id.spinner_type);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO: if is beer, change text to 'Enter brewery'/'flavor'; otherwise, change to 'Enter best location'/'ingredient'
                isBeer = (position == 0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void onSaveClicked(View v) {
        String name = ((EditText) findViewById(R.id.text_name)).getText().toString();
        String location = ((EditText) findViewById(R.id.text_location)).getText().toString();

        if (name == "") {
            promptLackInfoAlert("Please at lease give a name");
        } else {
            DataModel item;
            if (isBeer) {
                item = new Beer(name, "", imageFile, imgNeedRename, location, 0, "");
            } else {
                item = new Cocktail(name, "", imageFile, imgNeedRename, location, 0, "");
            }
            item.isWish = true;
            item.saveToDB();
            setResult(RESULT_OK);
            finish();
        }
    }
}
