package katie.alcoholic.helper;

import android.content.Context;
import android.graphics.Typeface;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * Created by liuyi on 2015-09-05.
 */
public class Helper {
    private static Typeface happyMonkeyRegular = null;
    private static Typeface stayPuft = null;

    public static DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).build();

    public static final int REQUEST_UPDATE = 1;
    public static final int REQUEST_ADD = 2;

    public static final String EXISTING_TIMESTAMP = "existing_timestamp";
    public static final String EXISTING_TABLE_NAME = "existing_table_name";
    // only used when item need to be edited
    public static final String EXISTING_DATA = "existing_data";

    public static Typeface getFontHMR(Context context) {
        if (happyMonkeyRegular == null) {
            happyMonkeyRegular = Typeface.createFromAsset(context.getAssets(), "fonts/HappyMonkey-Regular.ttf");
        }
        return happyMonkeyRegular;
    }

    public static Typeface getFontSP(Context context) {
        if (stayPuft == null) {
            stayPuft = Typeface.createFromAsset(context.getAssets(), "fonts/StayPuft.ttf");
        }
        return stayPuft;
    }
}
