package katie.alcoholic.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import katie.alcoholic.model.Beer;
import katie.alcoholic.model.Cocktail;
import katie.alcoholic.model.DataModel;

/**
 * Created by liuyi on 2015-08-09.
 */
public class DataManager extends SQLiteOpenHelper {

    public static final String TAG = "DataManger";

    private static final String DATABASE_NAME = "alcoholic.database";
    private static final int DATABASE_VERSIONT = 1;

    private static DataManager sInstance = null;

    public static void initializeSharedInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DataManager(context);
        }
    }

    public static DataManager sharedInstance() {
        return sInstance;
    }

    private DataManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSIONT);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createBeerTable(db);
        createCocktailTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void updateToDB(final String tableName, final String whereClause, final ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        if ((whereClause != null) && (whereClause.length() > 0) && existsField(tableName, whereClause)) {
            db.update(tableName, values, whereClause, null);
        } else {
            db.insert(tableName, null, values);
        }
    }

    public boolean existsField(final String tableName, final String predicateString) {
        final String SQLQUERY = "SELECT * FROM " + tableName + (predicateString.length() > 0 ? " WHERE " + predicateString : "");
        Cursor cursor = getReadableDatabase().rawQuery(SQLQUERY, null);
        final boolean retVal = (cursor.getCount() > 0);
        cursor.close();
        return retVal;
    }

    public ArrayList<DataModel> fetchAll(String tableName) {
        ArrayList<DataModel> models = new ArrayList<>();

        final String QUERY = "SELECT * FROM" + tableName;
        Cursor cursor = getReadableDatabase().rawQuery(QUERY, null);
        if (cursor.moveToFirst()) {
            do {
                DataModel model = null;
                if (tableName.equals(Beer.TABLE_NAME)) {
                    model = new Beer();
                } else {
                    model = new Cocktail();
                }
                model.loadFromCursor(cursor);
                models.add(model);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return models;
    }

    public <T> ArrayList<T> fetchWithName(String tableName, String predicateString, int fromPos, int toPos) {
        ArrayList<T> retVal = new ArrayList<>();

        //retrieving the cursor
        final String SQLQUERY = "SELECT * FROM " + tableName
                + ((predicateString != null) ? (" WHERE " + predicateString) : "")
                + " limit "+fromPos+", "+toPos;
        Cursor cursor = getReadableDatabase().rawQuery(SQLQUERY, null);

        //transfering the cursor into our model objects
        if (cursor.moveToFirst()) {
            do {
                DataModel model = null;
                if (tableName.equals(Beer.TABLE_NAME)) {
                    model = new Beer();
                } else {
                    model = new Cocktail();
                }
                model.loadFromCursor(cursor);
                retVal.add((T) model);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return retVal;
    }

    public DataModel fetchItemByTimestamp(String tableName, String timestamp) {
        final String SQLQuery = "SELECT * FROM " + tableName + " WHERE "
                + DataModel.TableFields.TIMSTAMP + "='" + timestamp + "'";
        Cursor cursor = getReadableDatabase().rawQuery(SQLQuery, null);
        DataModel model = null;
        if (cursor.moveToFirst()) {
            if (tableName.equals(Beer.TABLE_NAME)) {
                model = new Beer();
            } else {
                model = new Cocktail();
            }
            model.loadFromCursor(cursor);
        }
        return model;
    }

    public void removeFromDB(String tableName, String timestamp) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(tableName, DataModel.TableFields.TIMSTAMP + "=" + timestamp, null);
    }

    private void createCocktailTable(SQLiteDatabase database) {
        database.execSQL(createTableCommand(Cocktail.TABLE_NAME, Cocktail.TableFields.INGREDIENTS + " text"));
    }

    private void createBeerTable(SQLiteDatabase database) {
        database.execSQL(createTableCommand(Beer.TABLE_NAME, Beer.TableFields.FLAVOR + " text"));
    }

    private String createTableCommand(String tableName, String extraCommand) {
        return "create table " + tableName + " ("
                + DataModel.TableFields.NAME + " text not null, "
                + DataModel.TableFields.DESCRIPTION + " text, "
                + DataModel.TableFields.IMAGE_URL + " text, "
                + DataModel.TableFields.LOCATION + " text, "
                + DataModel.TableFields.RATING + " integer, "
                + DataModel.TableFields.TIMSTAMP + " text not null, "
                + DataModel.TableFields.FAVOURITE + " integer, "
                + DataModel.TableFields.WISH_LIST + " integer, "
                + extraCommand + ");";
    }
}
