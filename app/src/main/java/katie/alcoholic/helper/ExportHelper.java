package katie.alcoholic.helper;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import katie.alcoholic.model.Beer;
import katie.alcoholic.model.Cocktail;
import katie.alcoholic.model.DataModel;

public class ExportHelper {
    public static ExportHelper INSTANCE = new ExportHelper();

    public void exportAllToJSON(Context context) {
        JSONObject data = new JSONObject();
        try {
            data.put(Cocktail.TABLE_NAME, exportToJSON(Cocktail.TABLE_NAME));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            data.put(Beer.TABLE_NAME, exportToJSON(Beer.TABLE_NAME));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        saveToFile(context, data.toString());
    }

    private void saveToFile(Context context, String jsonString) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("liquidLunchData.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(jsonString);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private JSONArray exportToJSON(String tableName) {
        ArrayList<DataModel> models = DataManager.sharedInstance().fetchAll(tableName);
        JSONArray dataJsonArray = new JSONArray();
        for (DataModel model : models) {
            try {
                dataJsonArray.put(model.toJson());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return dataJsonArray;
    }

    public void log(String msg) {
        Log.i("katie", msg);
    }
}
