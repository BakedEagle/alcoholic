package katie.alcoholic;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

import katie.alcoholic.helper.DataManager;
import katie.alcoholic.helper.Helper;
import katie.alcoholic.model.Beer;
import katie.alcoholic.model.Cocktail;
import katie.alcoholic.model.DataModel;

/**
 * Created by liuyi on 2015-09-12.
 */
public class DetailActivity extends AppCompatActivity {
    private DataModel mData;
    private boolean isBeer;
    private boolean hasUpdated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        String timestamp = getIntent().getStringExtra(Helper.EXISTING_TIMESTAMP);
        String tableName = getIntent().getStringExtra(Helper.EXISTING_TABLE_NAME);
        mData = DataManager.sharedInstance().fetchItemByTimestamp(tableName, timestamp);

        isBeer = (mData instanceof Beer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(isBeer ? "Beer" : "Cocktail");
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(hasUpdated ? RESULT_OK : RESULT_CANCELED);
                finish();
            }
        });

        updateView();
    }

    private void updateView() {
        ((TextView) findViewById(R.id.detail_name)).setText(mData.getName());
        ((TextView) findViewById(R.id.detail_location)).setText(mData.getLocation());
        ((TextView) findViewById(R.id.detail_tag)).setText(mData.getTag());
        ((TextView) findViewById(R.id.detail_description)).setText(mData.getDescription());
        ((RatingBar) findViewById(R.id.detail_rating)).setRating(mData.getRating());
        ImageLoader.getInstance().displayImage(
                Uri.fromFile(new File(mData.getImagePath())).toString(),
                (ImageView) findViewById(R.id.detail_picture),
                Helper.defaultOptions
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_page, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == Helper.REQUEST_UPDATE) {
                String timestamp = data.getStringExtra(Helper.EXISTING_TIMESTAMP);
                mData = DataManager.sharedInstance().fetchItemByTimestamp(mData.getTableName(), timestamp);
                updateView();
                hasUpdated = true;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_item_edit:
                Intent intent = new Intent(this, AddCollectionActivity.class);
                intent.putExtra(Helper.EXISTING_DATA, mData);
                startActivityForResult(intent, Helper.REQUEST_UPDATE);
                break;
            case R.id.menu_item_delete:
                deleteItem();
                break;
            case R.id.menu_item_share:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Sorry, share feature is not available yet.")
                        .setNegativeButton("Ok", null)
                        .show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onShareClicked(View v){
        final Uri imageUri = Uri.fromFile(new File(mData.getImagePath()));
        final Activity activity = this;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String[] shareMenuItems = {"Share via Facebook", "Share via other applications"};
        builder.setTitle("Share Item")
                .setItems(shareMenuItems, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: // facebook
                                // TODO:
                                break;
                            case 1: // others
                                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                shareIntent.setType("text/plain, image/*");
                                shareIntent.putExtra(Intent.EXTRA_TEXT, mData.getDescription())
                                        .putExtra(Intent.EXTRA_STREAM, imageUri);
                                activity.startActivity(shareIntent);
                                break;
                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    private void deleteItem() {
        DataManager.sharedInstance().removeFromDB(isBeer ? Beer.TABLE_NAME : Cocktail.TABLE_NAME, mData.getTimestamp());
        setResult(RESULT_OK);
        finish();
    }
}
