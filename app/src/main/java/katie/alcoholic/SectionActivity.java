package katie.alcoholic;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import katie.alcoholic.helper.DataManager;
import katie.alcoholic.helper.ExportHelper;
import katie.alcoholic.helper.Helper;

/**
 * Created by liuyi on 2015-08-25.
 */
public class SectionActivity extends AppCompatActivity {
    public static final String TAG = "SectionActivity";
    public static final String PREF_FIRSTTIME = "first_time";
    public static final String PREF_NAME = "name";
    public static final String[] mCategories = {"My Collection", "My Favorite", "My Wish List"};

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListview;
    private ActionBarDrawerToggle mDrawerToggle;
    private TextView mToolBarTitle;
    private ViewPager mPager;
    private int lastSelectedDrawerItem = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section);

        DataManager.initializeSharedInstance(this);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.getBoolean(PREF_FIRSTTIME, true)) {
            askForName();
        }
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .build();
        ImageLoader.getInstance().init(config);

        mToolBarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolBarTitle.setTypeface(Helper.getFontSP(this));
        mPager = (ViewPager) findViewById(R.id.pager_main);
        setupDrawer();
        setupDrawerListView();

        if (savedInstanceState == null) {
            selectItem(1);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Helper.REQUEST_ADD || requestCode == Helper.REQUEST_UPDATE) {
                mPager.getAdapter().notifyDataSetChanged();
            }
        }
    }

    private void selectItem(int position) {
        if (position == (mCategories.length+1)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            AlertDialog alert = builder.setMessage("One of the greatest graphic designers of all time - the buffed girl, Yimotun\n\nAnd\n\nthe invisible developer, Katie")
                                        .setNegativeButton("Kiss you guys!", null)
                                        .setPositiveButton("Kiss more!", null)
                                        .setTitle("We are")
                                        .setIcon(R.drawable.crown)
                                        .show();

            TextView msgTxt = (TextView) alert.findViewById(android.R.id.message);
            msgTxt.setTextColor(getResources().getColor(R.color.drawer_button_activated));
            msgTxt.setGravity(Gravity.CENTER_HORIZONTAL);
            msgTxt.setTypeface(Helper.getFontHMR(this));
            mDrawerListview.setItemChecked(position, false);
            mDrawerListview.setItemChecked(lastSelectedDrawerItem, true);
            mDrawerLayout.closeDrawers();
        } else if (position == mCategories.length+2) {
            Toast.makeText(this, "Exporting...", Toast.LENGTH_SHORT).show();
            ExportHelper.INSTANCE.exportAllToJSON(this);
            Toast.makeText(this, "Finished", Toast.LENGTH_SHORT).show();
        } else {
            setTitle(mCategories[position - 1]);
            mPager.setAdapter(new MainPagerAdapter(getSupportFragmentManager(), position - 1));

            mDrawerListview.setItemChecked(position, true);
            mToolBarTitle.setText(mCategories[position - 1]);
            lastSelectedDrawerItem = position;
            mDrawerLayout.closeDrawers();
        }
    }

    private void setupDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.section_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                (Toolbar) findViewById(R.id.section_toolbar),
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.getToolbarNavigationClickListener();
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.setScrimColor(getResources().getColor(R.color.drawer_bg_transparent));
    }

    private void setupDrawerListView() {
        mDrawerListview = (ListView) findViewById(R.id.drawer);
        mDrawerListview.setAdapter(new DrawerListAdapter(this));
        mDrawerListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });
    }

    public void onAddClicked(View v) {
        Intent intent = new Intent(this, AddCollectionActivity.class);
        startActivityForResult(intent, Helper.REQUEST_ADD);
    }

    public void onWishClicked(View v) {
        Intent intent = new Intent(this, AddWishActivity.class);
        startActivityForResult(intent, Helper.REQUEST_ADD);
    }

    public void onLoadClicked(View v) {
        if (!((MainPagerAdapter) mPager.getAdapter()).fragments[mPager.getCurrentItem()].loadMoreData()) {
            ((RelativeLayout) v.getParent()).findViewById(R.id.no_more_data).setVisibility(View.VISIBLE);
            v.setVisibility(View.GONE);
        }
    }

    private void askForName() {
        final EditText input = new EditText(this);
        input.setGravity(Gravity.CENTER);
        FrameLayout container = new FrameLayout(this);
        FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int margin = (int) getResources().getDimension(R.dimen.alert_edittext_margin);
        params.leftMargin= margin;
        params.rightMargin = margin;
        input.setLayoutParams(params);
        container.addView(input);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(SectionActivity.this);
        alertDialog.setTitle("Who are you?")
                .setMessage("Please enter your name. Default name is 'Walter'. Note that you won't be able to change it once you enter.")
                .setView(container)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = input.getText().toString();
                        name = (name.length() > 0) ? name : "Walter";
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        preferences.edit().putBoolean(PREF_FIRSTTIME, false)
                                .putString(PREF_NAME, name)
                                .apply();
                        TextView nameView = (TextView) findViewById(R.id.header_name);
                        nameView.setText(name);
                    }
                })
                .show();
    }

    public class DrawerListAdapter extends BaseAdapter implements ListAdapter {
        private Context context;

        public DrawerListAdapter(Context c) {
            context = c;
        }

        @Override
        public int getCount() {
            return mCategories.length + 3;
        }

        @Override
        public Object getItem(int position) {
            return (position == 0) ? null : mCategories[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            return (position == 0) ? 0 : 1;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (position == 0) {
                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(R.layout.drawer_header, parent, false);

//                    Resources res = getResources();
//                    Bitmap imageBitmap = BitmapFactory.decodeResource(res, R.drawable.walter_profile);
//                    RoundedBitmapDrawable roundedImage = RoundedBitmapDrawableFactory.create(res, imageBitmap);
//                    roundedImage.setAntiAlias(true);
//                    roundedImage.setCornerRadius(imageBitmap.getHeight() / 2.0f);
//                    ((ImageView) convertView.findViewById(R.id.header_image)).setImageDrawable(roundedImage);
                    TextView name = (TextView) convertView.findViewById(R.id.header_name);
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    name.setText(preferences.getString(PREF_NAME, "Walter"));
                    name.setTypeface(Helper.getFontSP(context));
                }
            } else {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.drawer_item, parent, false);
                    TextView text = (TextView) convertView.findViewById(R.id.drawer_item_text);
                    text.setTypeface(Helper.getFontHMR(context));

                    if (position == (mCategories.length+1)){
                        text.setText("Creators");
                    } else if (position == mCategories.length+2) {
                        text.setText("Export");
                    } else {
                        text.setText(mCategories[position - 1]);
                    }
                }
            }
            return convertView;
        }

        @Override
        public boolean isEnabled(int position) {
            return position != 0;
        }
    }

    /**
     * PagerAdapter which displays Beer fragment on even positions, and
     * Cocktail fragment on odd positions.
     */
    public class MainPagerAdapter extends FragmentStatePagerAdapter {
        private int categoryPos = 0;
        SectionFragment[] fragments = {new SectionFragment(), new SectionFragment()};

        public MainPagerAdapter(android.support.v4.app.FragmentManager fm, int pos) {
            super(fm);
            categoryPos = pos;
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            Fragment fragment = fragments[position];
            Bundle args = new Bundle();
            args.putInt(SectionFragment.KEY_DRAWER_POS, categoryPos);
            args.putInt(SectionFragment.KEY_PAGER_POS, position);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return (position == 0) ? "Beer" : "Cocktail";
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }
}
