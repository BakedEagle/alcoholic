package katie.alcoholic;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by liuyi on 2015-09-12.
 */
public class AddItemActivity extends AppCompatActivity {
    protected static final int REQUEST_GALLERY = 1;
    protected static final int REQUEST_CAMERA = 2;

    public static final File imageDir = new File(Environment.getExternalStorageDirectory().toString(), "/Alcoholic/media/Alcoholic/");
    protected File imageFile = null;
    protected boolean imgNeedRename = true;
    protected boolean isBeer = true;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("What?!")
                    .setMessage("You haven't finished describing your new baby yet! Are you really going to leave like this?")
                    .setPositiveButton("Stay", null)
                    .setNegativeButton("Leave like a White Walker", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                imgNeedRename = true;
                if (imageFile.exists()) {
                    Bitmap imgBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                    Bitmap bitmapToSave = Bitmap.createScaledBitmap(imgBitmap, 1200, 1600, false);
                    saveLowQualityImage(bitmapToSave);
                    if (imageFile.exists()) {
                        ImageView imgView = (ImageView) findViewById(R.id.picture);
                        imgView.setImageBitmap(bitmapToSave);

                        MediaScannerConnection.scanFile(getApplicationContext(),
                                new String[]{imageFile.getAbsolutePath()},
                                null,
                                new MediaScannerConnection.OnScanCompletedListener() {
                                    @Override
                                    public void onScanCompleted(String path, Uri uri) {
                                    }
                                });
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Unable to save image. Please try again or load image from gallery.")
                            .setPositiveButton("Ok", null)
                            .show();
                }
            } else if (requestCode == REQUEST_GALLERY) {
                if (data == null || data.getData() == null)
                    return;

                // Pictures stored in different sub-directories in Gallery requires different way of
                // accessing the image file.
                // Two kinds of Uri we can get here
                // 1. "content://com.android.providers.media.documents/document"
                // 2. "content://media/external/images/media/"
                String selectedImagePath = getImagePath(data);
                if (selectedImagePath != null) {
                    File imgFile = new File(selectedImagePath);
                    if (imgFile.exists()) {
                        Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        if (imgBitmap.getWidth() > 1200) {
                            imgBitmap = Bitmap.createScaledBitmap(imgBitmap, 1200, 1600, false);
                            saveLowQualityImage(imgBitmap);
                            if (imageFile.exists()) {
                                imgNeedRename = true;
                            } else {
                                // Do not load image to imageview
                                imgNeedRename = false;
                                return;
                            }
                        } else {
                            imageFile = imgFile;
                            imgNeedRename = false;
                        }
                        ImageView imgView = (ImageView) findViewById(R.id.picture);
                        imgView.setImageBitmap(imgBitmap);
                    }
                }
            }
        }
    }

    protected void saveLowQualityImage(Bitmap bitmapToSave) {
        if (imageFile == null) {
            imageFile = createTmpImageFile();
        }
        FileOutputStream fileOut = null;

        try {
            fileOut = new FileOutputStream(imageFile);
            bitmapToSave.compress(Bitmap.CompressFormat.JPEG, 40, fileOut);
            fileOut.flush();
        } catch (IOException e) {
            e.printStackTrace();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Unable to save image. Please try again.")
                    .setPositiveButton("Ok", null)
                    .show();
        } finally {
            try {
                if (fileOut != null) {
                    fileOut.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected File createTmpImageFile() {
        if (!imageDir.isDirectory()) {
            imageDir.mkdirs();
        }
        return new File(imageDir, "tmp.jpg");
    }

    private String getImagePath(Intent data) {
        Uri dataUri = data.getData();
        String filePath = null;

        String dataString = data.getDataString();
        if (dataString.startsWith("content://com.android.providers.media.documents")) {
            String wholeID = DocumentsContract.getDocumentId(dataUri);
            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];
            String[] column = { MediaStore.Images.Media.DATA };
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel, new String[]{ id }, null);
            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } else if (dataString.startsWith("content://media")) {
            String[] proj = { MediaStore.Images.Media.DATA };
            CursorLoader cursorLoader = new CursorLoader(this, dataUri, proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();

            if(cursor != null){
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                filePath = cursor.getString(column_index);
            }
        }

        return filePath;
    }

    public void promptAddPhoto(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String[] photoSources = {"Camera", "Gallery"};
        builder.setTitle("Add A Photo")
                .setItems(photoSources, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0: //camera
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                                    imageFile = createTmpImageFile();
                                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
                                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                                }
                                break;
                            case 1: //gallery
                                Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(intent, REQUEST_GALLERY);
                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    public void onCancelClicked(View v) {
        setResult(RESULT_CANCELED);
        finish();
    }

    protected void promptLackInfoAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(message)
                .setPositiveButton("Ok", null)
                .show();
    }
}
