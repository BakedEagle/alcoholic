package katie.alcoholic.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by liuyi on 2015-08-09.
 */
public class Cocktail extends DataModel {
    public static final String TABLE_NAME = "table_cocktail";

    protected String ingredients = null;

    public Cocktail(){
        super();
    }

    public Cocktail(String name, String description, File imageFile, boolean needRename, String location, float rating, String ingredients) {
        super(name, description, imageFile, needRename, location, rating);
        this.ingredients = ingredients;
    }

    // TODO: sorting: ranking
    // TODO: filtering: location
    // TODO: searching: ingredients, description, name, location

    public static class TableFields {
        public static final String INGREDIENTS = "ingredients";
    }

    @Override
    public void loadFromCursor(Cursor cursor) {
        super.loadFromCursor(cursor);
        ingredients = cursor.getString(cursor.getColumnIndex(TableFields.INGREDIENTS));
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getTag() {
        return ingredients;
    }

    @Override
    protected void includeValuesToSave(ContentValues values, boolean includeTimeStamp) {
        super.includeValuesToSave(values, includeTimeStamp);
        values.put(TableFields.INGREDIENTS, ingredients);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = super.toJson();
        jsonObject.put(TableFields.INGREDIENTS, ingredients);
        return jsonObject;
    }

    @Override
    public void setTag(String tag) {
        this.ingredients = tag;
    }
}
