package katie.alcoholic.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by liuyi on 2015-08-09.
 */
public class Beer extends DataModel {
    public static final String TABLE_NAME = "table_beer";

    protected String flavor = null;

    public Beer() {
        super();
    }

    public Beer(String name, String description, File imageFile, boolean needRename, String location, float rating, String flavor) {
        super(name, description, imageFile, needRename, location, rating);
        this.flavor = flavor;
    }

    public static class TableFields {
        public static final String FLAVOR = "flavor";
    }

    @Override
    public void loadFromCursor(Cursor cursor) {
        super.loadFromCursor(cursor);
        flavor = cursor.getString(cursor.getColumnIndex(TableFields.FLAVOR));
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getTag() {
        return flavor;
    }

    @Override
    protected void includeValuesToSave(ContentValues values, boolean includeTimeStamp) {
        super.includeValuesToSave(values, includeTimeStamp);
        values.put(TableFields.FLAVOR, flavor);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = super.toJson();
        jsonObject.put(TableFields.FLAVOR, flavor);
        return jsonObject;
    }

    @Override
    public void setTag(String tag) {
        this.flavor = tag;
    }
}
