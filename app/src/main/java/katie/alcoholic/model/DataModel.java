package katie.alcoholic.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import katie.alcoholic.AddCollectionActivity;
import katie.alcoholic.helper.DataManager;

/**
 * Created by liuyi on 2015-09-10.
 */
public abstract class DataModel implements Serializable {
    protected String name = null;
    protected String description = null;
    protected String imagePath = null;
    protected float rating;
    protected String location = null;
    protected String timestamp;
    public boolean isFavorite = false;
    public boolean isWish = false;

    protected DataModel() {}

    protected DataModel(String name, String description, File imageFile, boolean needRename, String location, float rating) {
        this.name = name;
        this.description = description;
        this.location = location;
        this.rating = rating;

        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyMMddHHmmss");
        this.timestamp = df.format(date);

        if (imageFile == null) {
            this.imagePath = "";
        } else {
            finalizeImgFile(imageFile, needRename);
        }
    }

    public void finalizeImgFile(File imageFile, boolean needRename) {
        if (needRename) {
            File newPath = new File(AddCollectionActivity.imageDir, timestamp + ".jpg");
            if (newPath.exists()) {
                newPath.delete();
            }
            imageFile.renameTo(newPath);
            this.imagePath = newPath.getAbsolutePath();
        } else {
            this.imagePath = imageFile.getAbsolutePath();
        }
    }

    public static class TableFields {
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String IMAGE_URL = "image_url";
        public static final String RATING = "rating";
        public static final String LOCATION = "location";
        public static final String TIMSTAMP = "timestamp";
        public static final String FAVOURITE = "is_favourite";
        public static final String WISH_LIST = "is_wish_list";
    }

    public void loadFromCursor(Cursor cursor) {
        name = cursor.getString(cursor.getColumnIndex(TableFields.NAME));
        description = cursor.getString(cursor.getColumnIndex(TableFields.DESCRIPTION));
        imagePath = cursor.getString(cursor.getColumnIndex(TableFields.IMAGE_URL));
        rating = cursor.getFloat(cursor.getColumnIndex(TableFields.RATING));
        location = cursor.getString(cursor.getColumnIndex(TableFields.LOCATION));
        timestamp = cursor.getString(cursor.getColumnIndex(TableFields.TIMSTAMP));
        isFavorite = cursor.getInt(cursor.getColumnIndex(TableFields.FAVOURITE)) == 1;
        isWish = cursor.getInt(cursor.getColumnIndex(TableFields.WISH_LIST)) == 1;
    }

    /**
     * Save this new item into database, including its timestamp
     */
    public void saveToDB() {
        ContentValues values = new ContentValues();
        includeValuesToSave(values, true);
        saveToDB(values);
    }

    /**
     * Save this item into database. Updating its timestamp is optional
     */
    public void updateToDB() {
        ContentValues values = new ContentValues();
        includeValuesToSave(values, false);
        saveToDB(values);
    }

    public void saveToDB(ContentValues values) {
        try {
            DataManager.sharedInstance().updateToDB(getTableName(), getWhereClauseString(), values);
        } catch (Exception e) {
            Log.i("", "Exception caught in saveToDB with message : " + e);
        }
    }

    public void updateFavoriteValue() {
        try {
            ContentValues values = new ContentValues();
            values.put(TableFields.FAVOURITE, isFavorite ? 1 : 0);
            DataManager.sharedInstance().updateToDB(getTableName(), getWhereClauseString(), values);
        } catch (Exception e) {
            Log.i("", "Exception caught in updateFavoriteValue with message : " + e);
        }
    }

    protected void includeValuesToSave(ContentValues values, boolean includeTimeStamp) {
        values.put(TableFields.NAME, name);
        values.put(TableFields.DESCRIPTION, description);
        values.put(TableFields.IMAGE_URL, imagePath);
        values.put(TableFields.RATING, rating);
        values.put(TableFields.LOCATION, location);
        values.put(TableFields.TIMSTAMP, timestamp);
        values.put(TableFields.FAVOURITE, isFavorite ? 1 : 0);
        values.put(TableFields.WISH_LIST, isWish ? 1 : 0);
    }

    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(TableFields.NAME, name);
        jsonObject.put(TableFields.DESCRIPTION, description);
        jsonObject.put(TableFields.IMAGE_URL, imagePath);
        jsonObject.put(TableFields.RATING, rating);
        jsonObject.put(TableFields.LOCATION, location);
        jsonObject.put(TableFields.TIMSTAMP, timestamp);
        jsonObject.put(TableFields.FAVOURITE, isFavorite);
        jsonObject.put(TableFields.WISH_LIST, isWish);
        return jsonObject;
    }

    public String getWhereClauseString() {
        return (TableFields.TIMSTAMP + "='" + timestamp + "'");
    }

    public abstract String getTableName();
    public abstract String getTag();
    public abstract void setTag(String tag);

    public String getName() {
        return name;
    }
    public String getImagePath() {
        return imagePath;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public float getRating() {
        return rating;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTimestamp() {
        return timestamp;
    }

}
