package katie.alcoholic;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import katie.alcoholic.helper.DataManager;
import katie.alcoholic.model.Beer;
import katie.alcoholic.model.Cocktail;
import katie.alcoholic.model.DataModel;

/**
 * Created by liuyi on 2015-08-25.
 */
public class SectionFragment extends Fragment {
    public static final String TAG = "SectionFragment";
    public static final String KEY_DRAWER_POS = "DRAWER_POSITION";
    public static final String KEY_PAGER_POS = "PAGER_POSITION";

    public static final int INDEX_COLLECTION = 0;
    public static final int INDEX_FAVOURITE = 1;
    public static final int INDEX_WISHLIST = 2;

    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private boolean isBeer;
    private int categoryIndex = 0;
    private int dataStartingPos = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_section, container, false);

        Bundle arguments = getArguments();
        categoryIndex = arguments.getInt(KEY_DRAWER_POS);
        isBeer = (arguments.getInt(KEY_PAGER_POS) == 0);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_main);
        mRecyclerView.setHasFixedSize(true); // this can improve performance
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new RecyclerAdapter(getActivity(), populateData(), categoryIndex==INDEX_WISHLIST, categoryIndex==INDEX_FAVOURITE);
        mRecyclerView.setAdapter(mAdapter);

        return rootView;
    }

    public boolean loadMoreData() {
        dataStartingPos = dataStartingPos + 10;
        ArrayList<DataModel> moreData = populateData();
        if (moreData.size() > 0) {
            mAdapter.addMoreData(moreData);
            return true;
        } else {
            return false;
        }
    }

    private ArrayList<DataModel> populateData() {
        String tableName = isBeer ? Beer.TABLE_NAME : Cocktail.TABLE_NAME;
        ArrayList<DataModel> result = null;
        switch (categoryIndex) {
            case 0: // collection
                result = DataManager.sharedInstance().fetchWithName(
                        tableName, DataModel.TableFields.WISH_LIST+"='0'",
                        dataStartingPos, dataStartingPos+9);
                break;
            case 1: // fav
                result = DataManager.sharedInstance().fetchWithName(
                        tableName, DataModel.TableFields.FAVOURITE+"='1'",
                        dataStartingPos, dataStartingPos+9);
                break;
            case 2: // wish list
                result = DataManager.sharedInstance().fetchWithName(
                        tableName, DataModel.TableFields.WISH_LIST+"='1'",
                        dataStartingPos, dataStartingPos+9);
                break;
        }
        return result;
    }
}
