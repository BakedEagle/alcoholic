package katie.alcoholic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.util.ArrayList;

import at.markushi.ui.CircleButton;
import katie.alcoholic.helper.Helper;
import katie.alcoholic.model.DataModel;

/**
 * Created by liuyi on 2015-09-06.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    public static final String TAG = "RecyclerAdapter";

    protected ArrayList<DataModel> mDataSet;
    protected Context context;
    protected boolean isWishList;
    protected boolean isFavList;

    public RecyclerAdapter(Context ctx, ArrayList<DataModel> dataSet, boolean isWish, boolean isFav) {
        context = ctx;
        mDataSet = dataSet;
        isWishList = isWish;
        isFavList = isFav;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_list_item, viewGroup, false);
        ViewHolder holder = new ViewHolder((RelativeLayout) v, (Activity) context);
        v.setOnClickListener(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int pos) {
        if (pos < mDataSet.size()) {
            viewHolder.mLoadButton.setVisibility(View.GONE);
            viewHolder.mNoDataMsg.setVisibility(View.GONE);
            viewHolder.mCard.setVisibility(View.VISIBLE);

            DataModel data = mDataSet.get(pos);
            viewHolder.updateData(data);
            viewHolder.mTitle.setText(data.getName());

            String location = data.getLocation();
            if (location.length() > 0) {
                viewHolder.mLocation.setVisibility(View.VISIBLE);
                viewHolder.mLocation.setText(data.getLocation());
            } else {
                viewHolder.mLocation.setVisibility(View.GONE);
            }

            if (!isWishList) {
                viewHolder.mSaveIcon.setVisibility(View.GONE);

                viewHolder.mRating.setRating(data.getRating());

                ImageLoader.getInstance().displayImage(
                        Uri.fromFile(new File(data.getImagePath())).toString(),
                        viewHolder.mImage,
                        Helper.defaultOptions);

                if (viewHolder.mFavClickListener == null) {
                    viewHolder.mFavClickListener = new OnFavClickedListener(data, viewHolder);
                    viewHolder.mFavIcon.setOnClickListener(viewHolder.mFavClickListener);
                } else {
                    viewHolder.mFavClickListener.update(data, viewHolder);
                }
                if (data.isFavorite) {
                    viewHolder.mFavIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_red));
                } else {
                    viewHolder.mFavIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_white));
                }
            } else {
                viewHolder.mRating.setVisibility(View.GONE);
                viewHolder.mFavIcon.setVisibility(View.INVISIBLE);

                if (data.getImagePath().length() > 0) {
                    ImageLoader.getInstance().displayImage(
                            Uri.fromFile(new File(data.getImagePath())).toString(),
                            viewHolder.mImage,
                            Helper.defaultOptions);
                } else {
                    viewHolder.mImage.setImageDrawable(context.getResources().getDrawable(R.drawable.add_image));
                }

                if (viewHolder.mSaveClickListener == null) {
                    viewHolder.mSaveClickListener = new OnSaveClickedListener(data);
                    viewHolder.mSaveIcon.setOnClickListener(viewHolder.mSaveClickListener);
                } else {
                    viewHolder.mSaveClickListener.update(data);
                }
            }
        } else {
            viewHolder.mCard.setVisibility(View.GONE);
            viewHolder.mNoDataMsg.setVisibility(View.GONE);
            viewHolder.mLoadButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size() + 1;
    }

    public void addMoreData(ArrayList<DataModel> moreData) {
        int originalSize = mDataSet.size();
        // this is supposetly faster than mDataSet.addAll(moreData)
        for (DataModel data : moreData) {
            mDataSet.add(data);
        }
        notifyItemRangeChanged(originalSize, moreData.size());
    }

    private void removeItem(int pos) {
        mDataSet.remove(pos);
        notifyItemRemoved(pos);
        notifyItemRangeChanged(pos-1, mDataSet.size());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private DataModel mData;
        private Activity activity;

        public CardView mCard;
        public TextView mTitle;
        public TextView mLocation;
        public ImageView mImage;
        public ImageView mFavIcon;
        public OnFavClickedListener mFavClickListener;
        public RatingBar mRating;
        public ImageView mSaveIcon;
        public OnSaveClickedListener mSaveClickListener;
        public CircleButton mLoadButton;
        public TextView mNoDataMsg;

        public ViewHolder(RelativeLayout itemView, Activity act) {
            super(itemView);
            activity = act;
            mCard = (CardView) itemView.findViewById(R.id.card_view);
            mTitle = (TextView) itemView.findViewById(R.id.card_title);
            mLocation = (TextView) itemView.findViewById(R.id.card_loc);
            mImage = (ImageView) itemView.findViewById(R.id.card_image);
            mFavIcon = (ImageView) itemView.findViewById(R.id.card_fav_icon);
            mSaveIcon = (ImageView) itemView.findViewById(R.id.card_save_icon);
            mRating = (RatingBar) itemView.findViewById(R.id.card_rating);
            mLoadButton = (CircleButton) itemView.findViewById(R.id.load_button);
            mNoDataMsg = (TextView) itemView.findViewById(R.id.no_more_data);
        }

        public void updateData(DataModel data) {
            mData = data;
        }

        @Override
        public void onClick(View v) {
            if (mCard.getVisibility() == View.VISIBLE) {
                Intent intent = new Intent(activity, DetailActivity.class);
                intent.putExtra(Helper.EXISTING_TIMESTAMP, mData.getTimestamp());
                intent.putExtra(Helper.EXISTING_TABLE_NAME, mData.getTableName());
                activity.startActivityForResult(intent, Helper.REQUEST_UPDATE);
            }
        }
    }

    private class OnFavClickedListener implements View.OnClickListener {
        private DataModel data = null;
        private ViewHolder holder;

        public OnFavClickedListener(DataModel data, ViewHolder holder) {
            this.data = data;
            this.holder = holder;
        }

        public void update(DataModel newData, ViewHolder newHolder) {
            data = newData;
            holder = newHolder;
        }

        @Override
        public void onClick(View v) {
            if (data.isFavorite) {
                holder.mFavIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_white));
                if (isFavList) {
                    int pos = holder.getPosition();
                    removeItem(pos);
                }
            } else {
                holder.mFavIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_red));
            }
            data.isFavorite = !data.isFavorite;
            data.updateFavoriteValue();
        }
    }

    private class OnSaveClickedListener implements View.OnClickListener {
        private DataModel data = null;

        public OnSaveClickedListener(DataModel data) {
            this.data = data;
        }

        public void update(DataModel newData) {
            data = newData;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, AddCollectionActivity.class);
            intent.putExtra(Helper.EXISTING_DATA, data);
            context.startActivity(intent);
        }
    }
}